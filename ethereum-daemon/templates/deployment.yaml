apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "ethereum-daemon.fullname" . }}
  labels:
    {{- include "ethereum-daemon.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "ethereum-daemon.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "ethereum-daemon.selectorLabels" . | nindent 8 }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}

      {{- if .Values.hostNetwork }}
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      {{- end }}

      serviceAccountName: {{ include "ethereum-daemon.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      initContainers:
        - name: init-scripts
          image: "{{ .Values.image.scripts.repository }}:{{ .Values.image.scripts.tag }}"
          imagePullPolicy: {{ .Values.image.scripts.pullPolicy }}
          volumeMounts:
            - name: scripts
              mountPath: /tmp
          command: [ "sh", "-c", "cp /scripts/* /tmp" ]
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.eth.repository }}:{{ .Values.image.eth.tag }}"
          imagePullPolicy: {{ .Values.image.eth.pullPolicy }}
          {{- if eq .Values.net "mocknet" }}
          command: [ "sh", "-c", "apk add curl && /scripts/ethereum-mock.sh" ]
          {{- else }}
          command: [ "sh", "-c", 'geth --rinkeby --syncmode fast -rpc --rpcaddr 0.0.0.0 --rpcport 8545 --rpcapi "eth,net,web3,miner,personal,txpool,debug" --rpccorsdomain "*" --rpcvhosts="*"' ]
          {{- end }}
          volumeMounts:
            - name: scripts
              mountPath: /scripts
            - name: data
              mountPath: /root
          ports:
            - name: rpc
              containerPort: 8545
              protocol: TCP
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: data
        {{- if and .Values.persistence.enabled (not .Values.persistence.hostPath) }}
          persistentVolumeClaim:
            claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "ethereum-daemon.fullname" . }}{{- end }}
        {{- else if and .Values.persistence.enabled .Values.persistence.hostPath }}
          hostPath:
            path: {{ .Values.persistence.hostPath }}
            type: DirectoryOrCreate
        {{- else }}
          emptyDir: {}
        {{- end }}
        - name: scripts
          emptyDir: {}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
